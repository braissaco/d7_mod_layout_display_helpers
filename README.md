# Layout display Helpers #

## Módulo de ayuda a la maquetación para Drupal 7 ##



![general.PNG](https://bitbucket.org/repo/R9949M8/images/540051797-general.PNG)


![general_XS.PNG](https://bitbucket.org/repo/R9949M8/images/689049662-general_XS.PNG)



## Herramientas ##

El objetivo del módulo es proporcionar algunas herramientas para facilitar el trabajo de maquetación en Drupal 7. Se compone de 3 herramientas muy simples que pueden activarse y desactivarse desde el panel de configuración del módulo:

### Layout y puntos de corte ###

Mostrar dinámicamente el **"layout" actual**. Se basa en los puntos de corte establecidos en Bootstrap 3, y permite comprobar en todo momento en cúal de los "layouts" se está visualizando la página (LG, MD, SM, XS). 

Se pueden añadir otros dos puntos de corte: Retina (RT) y Tiny (XXS).

  ![layout.PNG](https://bitbucket.org/repo/R9949M8/images/3669114364-layout.PNG)


### Tamaño de la ventana ###

Mostrar dinámicamente el **tamaño actual** de la ventana en pixels, con un indicador [ancho x alto].

  ![tamano.PNG](https://bitbucket.org/repo/R9949M8/images/1752076786-tamano.PNG)


### Identificación del sitio ###

Mostrar información sobre el **sitio o entorno** Drupal en el cual nos encontramos. Esto es útil cuando estamos trabajando a la vez con varios sitios o con varios entornos para el mismo proyecto y queremos comprobar rápidamente en cuál de ellos estamos trabajando en ese momento. Para identificar el sitio disponemos de:

* Visor de sitio en la parte inferior izquierda.

  ![visor.PNG](https://bitbucket.org/repo/R9949M8/images/272045010-visor.PNG)

* Prefijo del sitio en el TITLE de la ventana y por lo tanto visible en los títulos de las pestañas.

  ![tabs.PNG](https://bitbucket.org/repo/R9949M8/images/3455507454-tabs.PNG)


*NOTA: Por defecto, el módulo obtendrá la información del sitio a través de su URL, pudiendo ser modificada a través de la configuración del módulo.*


### Otras características ###

El toolkit puede ocultarse, si es necesario, utilizando el botón [X] situado en la parte inferior derecha. Este permanecerá replegado hasta que se vuelva a abrir empleando de nuevo el botón, incluso aunque se recargue el navegador ya que se almacena en el localstorage del mismo el estado actual del toolkit (visible / oculto).

![hide.PNG](https://bitbucket.org/repo/R9949M8/images/2196040944-hide.PNG)      ![show.PNG](https://bitbucket.org/repo/R9949M8/images/68839222-show.PNG)


## Permisos ##

Pueden gestionarse los permisos para configurar y mostrar las herramientas a través de la configuración de permisos de Drupal:

**Layout Display Helpers** | *Access content for the Layout Display Helpers module*


## Configuración ##

Se puede acceder a la configuración del módulo desde el menú: **Configuración -> Desarrollo -> Layout Display Helpers**

**Enable Displays:** Activa / Desactiva todas las herramientas.

**Menú Screen Layouts:**

  * Active Screen Layouts Display: Desactiva la herramienta de mostrar layout actual.
  * Bootstrap Layouts: Mostrar los Layouts de Bootstrap.
  * More Layouts: Mostrar layouts extra: Retina (529px) y Tiny (320px)
  * Themes: Temas para los que está activa esta herramienta.

**Menú Window Measures:**

  * Active Window Measures Display: Desactiva la herramienta de mostrar el tamaño de la ventana.
  * Themes: Temas para los que está activa esta herramienta.

**Menú Site Information:**

  * Active Site Information Display: Desactiva la herramienta de mostrar información del entorno de trabajo.
  * Site Identifier: Identificador del sitio
  * Key: Nombre abreviado identificativo del sitio.
  * RGB Color: color en formato #HEX para el visor del sitio.
  * Themes: Temas para los que está activa esta herramienta.

**Reset Environment:** Resetea los parámetros a los valores por defecto.



## Posibles Mejoras ##

* Añadir puntos de corte personalizados