/**
 * Layout_display_Helpers Class.
 * @author Brais Saco Estévez
 * 
 * JS Manager for Layout Display Helper Module
 */
var LayoutDisplayHelpers = function($, settings) {

  var self = this;

  /*
   * ATTRIBUTES
   * **************************************************************************
   */
  /** This class name * */
  self.className = 'LayoutDisplayHelpers';

  /** UI elements * */
  self.ui = {};

  /** Animation parameters * */
  self.animate = {
    initial_delay : 200,
    fade_timing : 300
  };

  /** State of the displays wrapper: open or closed * */
  self.open = 1;

  /*
   * PUBLIC METHODS
   * **************************************************************************
   */

  /**
   * Initialize this Class.
   */
  self.init = function() {
    var show = !$("body").hasClass("layd-total-off");
    if (!show) return;
    checkState();
    initUi();
    initBehaviour();
  };

  /*
   * PRIVATE METHODS
   * **************************************************************************
   */

  /**
   * Checks for wrapper state: open or closed.
   */
  var checkState = function () {
    if (!localStorage) return;
    var state = localStorage.getItem("ld_state");
    if (state != null) self.open = state;
  };

  /**
   * Initializes Module UI elements.
   */
  var initUi = function() {

    // Layout Helpers Elements.
    self.ui.wrapper = $("<div></div>", {
      id : "layd-wrapper"
    }).appendTo("body");
    self.ui.siteInformation = $("#layd-site-info");
    self.ui.layoutMeasures = $("#layd-measures-info");
    self.ui.siteInformation.appendTo(self.ui.wrapper);
    self.ui.layoutMeasures.appendTo(self.ui.wrapper);

    // Add Actions Buttons to layout display helpers main wrapper.
    initButtons();

    // Admin Form elements.
    self.ui.adminResetEnvButton = $("#edit-reset-env-button");

    //Hide helpers if persistent state is 'hidden'.
    if (self.open == 0 && self.ui.toggleButton && self.ui.toggleButton.length) {
      hideWrapper(false);
    }

    // Show wrappper.
    self.ui.wrapper
      .delay(self.animate.initial_delay)
      .fadeIn(self.animate.fade_timing);

  };

  /**
   * Initializes action buttons.
   */
  var initButtons = function() {
    // Toggle visibility of display's helpers if there's something to hide/show.
    var totalOff = $("body").hasClass("layd-total-off");
    var screenDspOff = $("body").hasClass("layd-off");
    var numDisplays = self.ui.wrapper.children().length;

    if (!totalOff && (!screenDspOff || numDisplays)) {
      self.ui.toggleButton = $("<div></div>", {
        id : "layd-toogle-btn"
      }).click(function() {
        if (!self.open) showWrapper();
        else hideWrapper();
      });
      self.ui.wrapper.append(self.ui.toggleButton);
    }

  };

  /**
   * Initializes UI elements dynamic behaviours.
   */
  var initBehaviour = function() {
    initMeasuresDisplay();
    initAdminForm();
  };

  /**
   * Initializes Window measures display.
   */
  var initMeasuresDisplay = function() {
    setWindowMeasuresOnDisplay();
    $(window).resize(setWindowMeasuresOnDisplay);
  };

  /**
   * Write the size on Window measures display.
   */
  var setWindowMeasuresOnDisplay = function() {
    var measures = getWindowMeasures();
    self.ui.layoutMeasures.text(measures[0] + " x " + measures[1]);
  };

  /**
   * Get actual size of the window.
   * 
   * @return Array Window size (Width, Height)
   */
  var getWindowMeasures = function() {
    var width = window.innerWidth;
    var height = window.innerHeight;
    return [ width, height ];
  };

  /**
   * Initialize module's administration form elements.
   */
  var initAdminForm = function() {
    // Confirm before submit "Reset environment" action.
    self.ui.adminResetEnvButton.click(function() {
      var data = $(this).data();
      if (!confirm(data.confirmText)) {
        return false;
      }
    })
  };

  /**
   * Hide helper's wrapper.
   */
  var hideWrapper = function (animation) {
    var animate = typeof animation === "undefined" ? true : animation;
    var timing = animate ? self.animate.fade_timing : 0;
    self.ui.wrapper.addClass("layd-hidden");
    self.ui.toggleButton.addClass("to-show");
    self.ui.wrapper.children().not(self.ui.toggleButton).fadeOut(timing);
    self.open = 0;
    if (localStorage) localStorage.setItem("ld_state", self.open);
  };

  /**
   * Show helper's wrapper.
   */
  var showWrapper = function (animation) {
    var animate = typeof animation === "undefined" ? true : animation;
    var timing = animate ? self.animate.fade_timing : 0;
    self.ui.wrapper.removeClass("layd-hidden");
    self.ui.toggleButton.removeClass("to-show");
    self.ui.wrapper.children().not(self.ui.toggleButton).fadeIn(timing);
    self.open = 1;
    if (localStorage) localStorage.setItem("ld_state", self.open);
  };


  /* SELF INIT CLASS */
  self.init();

  return self;

};

/**
 * Build Module Manager Object on document Ready (If JQuery exists).
 * 
 * @type {{attach: Drupal.behaviors.layoutDisplay.attach}}
 */
 
var jqPlugin;
if (typeof $ !== "undefined") jqPlugin = $;
else if (typeof $jq !== "undefined") jqPlugin = $jq;
else if (typeof jq !== "undefined") jqPlugin = jq;
else if (typeof jQuery !== "undefined") jqPlugin = jQuery;
 
if (jqPlugin) {
  Drupal.behaviors.layoutDisplay = {
    attach : function(context, settings) {
      if (settings.layout_display) {
        jqPlugin('body', context).once('layoutDisplay', function () {
          settings.layout_display.manager =
            new LayoutDisplayHelpers(jqPlugin, settings);
        });
      }
    }
  };
}
