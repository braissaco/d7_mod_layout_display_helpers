<?php

/**
 * Define Screen Layout display configuration form's parameters.
 *
 * @param Array $form
 *        Configuration form.
 * @param Array $defaults
 *        Default values for config parameters.
 * @param Array $storage
 *        Stored values for parameters.
 * @param Array $enabled_themes
 *        Enabled themes for this site.
 * @param Array $default_activated_themes
 *        Default themes that are enabled for this feature.
 */
function _layout_display_form_screen_layout(&$form, $defaults, $storage, $enabled_themes, $default_activated_themes) {
  
  $defaults = LayoutDisplay::get_defaults();
  $storage = LayoutDisplay::init_storage();
  
  // Layouts options.
  $form['module_opts']['layouts'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Screen Layouts')
  );
  // Activate / Deactivate Layout Displays
  $form['module_opts']['layouts']['layout_display_active'] = array(
    '#title' => t('Active Screen Layouts Display'), 
    '#description' => t('Turn on/off screen layouts display feature'), 
    '#type' => 'checkbox', 
    '#default_value' => (!isset($storage['config']['layout_info']['active'])) ? $defaults['config_layout_active'] : $storage['config']['layout_info']['active']
  );
  // Layouts options -> Avaliable.
  $form['module_opts']['layouts']['avaliable'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Avaliable Screen Layouts')
  );
  $form['module_opts']['layouts']['avaliable']['layout_display_active_bootstrap'] = array(
    '#title' => t('Bootstrap Layouts'), 
    '#description' => t('Show Bootstrap pre-defined screen layouts: extra-small devices (XS), Small devices (SM), Medium devices (MD) and Wide (LG).'),
    '#type' => 'checkbox', 
    '#default_value' => (!isset($storage['config']['layout_info']['bootstrap'])) ? $defaults['config_layout_active_bootstrap'] : $storage['config']['layout_info']['bootstrap']
  );
  $form['module_opts']['layouts']['avaliable']['layout_display_active_extra'] = array(
    '#title' => t('More Layouts'),
    '#description' => t('Show smallest mobile devices screen layouts: Tiny devices (XXS) and Retina Devices (RT)'),
    '#type' => 'checkbox', 
    '#default_value' => (!isset($storage['config']['layout_info']['extra'])) ? $defaults['config_layout_active_extra'] : $storage['config']['layout_info']['extra']
  );
  // Layouts options -> Themes.
  $form['module_opts']['layouts']['themes'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Themes')
  );
  $form['module_opts']['layouts']['themes']['layout_display_activated_themes'] = array(
    '#type' => 'checkboxes', 
    '#options' => drupal_map_assoc($enabled_themes), 
    '#description' => t('Themes where screen layout display is avaliable.'), 
    '#default_value' => (isset($storage['config']['layout_info']['themes'])) ? $storage['config']['layout_info']['themes'] : $default_activated_themes
  );
}

/**
 * Define Window Measures display configuration form's parameters.
 *
 * @param Array $form
 *        Configuration form.
 * @param Array $defaults
 *        Default values for config parameters.
 * @param Array $storage
 *        Stored values for parameters.
 * @param Array $enabled_themes
 *        Enabled themes for this site.
 * @param Array $default_activated_themes
 *        Default themes that are enabled for this feature.
 */
function _layout_display_form_window_measures(&$form, $defaults, $storage, $enabled_themes, $default_activated_themes) {
  
  $defaults = LayoutDisplay::get_defaults();
  $storage = LayoutDisplay::init_storage();
  
  // Layout Measures info options.
  $form['module_opts']['measures'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Window Measures')
  );
  // Activate / Deactivate Layout Measures info
  $form['module_opts']['measures']['measures_active'] = array(
    '#title' => t('Active Window Measures Display'), 
    '#description' => t('Turn on/off window measures display feature'), 
    '#type' => 'checkbox', 
    '#default_value' => (!isset($storage['config']['measures_info']['active'])) ? $defaults['config_measures_active'] : $storage['config']['measures_info']['active']
  );
  // Layouts measures -> Themes.
  $form['module_opts']['measures']['themes'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Themes')
  );
  $form['module_opts']['measures']['themes']['measures_activated_themes'] = array(
    '#type' => 'checkboxes', 
    '#options' => drupal_map_assoc($enabled_themes), 
    '#description' => t('Themes where window measures display is avaliable.'), 
    '#default_value' => (isset($storage['config']['measures_info']['themes'])) ? $storage['config']['measures_info']['themes'] : $default_activated_themes
  );
}

/**
 * Define Site Information display configuration form's parameters.
 *
 * @param Array $form
 *        Configuration form.
 * @param Array $defaults
 *        Default values for config parameters.
 * @param Array $storage
 *        Stored values for parameters.
 * @param Array $enabled_themes
 *        Enabled themes for this site.
 * @param Array $default_activated_themes
 *        Default themes that are enabled for this feature.
 */
function _layout_display_form_site_information(&$form, $defaults, $storage, $enabled_themes, $default_activated_themes) {

  //Set default themes -> ALL ENABLED THEMES.
  $default_activated_themes = $enabled_themes;

  // Site Information options.
  $form['module_opts']['site_info'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Site Information')
  );
  // Activate / Deactivate Site Information
  $form['module_opts']['site_info']['site_info_active'] = array(
    '#title' => t('Active Site Information Display'), 
    '#description' => t('Turn on/off site information display feature'), 
    '#type' => 'checkbox', 
    '#default_value' => (!isset($storage['config']['site_info']['active'])) ? $defaults['config_site_active'] : $storage['config']['site_info']['active']
  );
  // Site Information options -> Identifier.
  $form['module_opts']['site_info']['info'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Site information display')
  );
  $form['module_opts']['site_info']['info']['site_info_identifier'] = array(
    '#title' => t('Site Identifier'), 
    '#description' => t('Identifier for the site. The text will be shown in the display.'), 
    '#type' => 'textfield', 
    '#size' => 50, 
    '#maxlength' => 50, 
    '#default_value' => (!isset($storage['config']['site_info']['identifier'])) ? $defaults['config_site_identifier'] : $storage['config']['site_info']['identifier']
  );
  $form['module_opts']['site_info']['info']['site_info_key'] = array(
    '#title' => t('Key'), 
    '#description' => t('Abbreviated text used as a key for the site.'), 
    '#type' => 'textfield', 
    '#size' => 7, 
    '#maxlength' => 7, 
    '#default_value' => (!isset($storage['config']['site_info']['key'])) ? $defaults['config_site_key'] : $storage['config']['site_info']['key']
  );
  $form['module_opts']['site_info']['info']['site_info_color'] = array(
    '#title' => t('RGB Color'), 
    '#description' => t('Background color for the site identifier (RGB Format without "#" character).'), 
    '#field_prefix' => '#', 
    '#type' => 'textfield', 
    '#size' => 10, 
    '#maxlength' => 6, 
    '#default_value' => (!isset($storage['config']['site_info']['color'])) ? $defaults['config_site_color'] : $storage['config']['site_info']['color']
  );
  // Site Information options -> Themes.
  $form['module_opts']['site_info']['themes'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Themes')
  );
  $form['module_opts']['site_info']['themes']['site_info_activated_themes'] = array(
    '#type' => 'checkboxes', 
    '#options' => drupal_map_assoc($enabled_themes), 
    '#description' => t('Themes where site information display is avaliable.'), 
    '#default_value' => (isset($storage['config']['site_info']['themes'])) ? $storage['config']['site_info']['themes'] : $default_activated_themes
  );
}
