<?php
/**
 * LayoutDisplay Main class
 * 
 * @author Brais Saco Estevez
 */
class LayoutDisplay {
  
  /**
   * @var $module_id string Module identifier.
   */
  private static $module_id = 'layout_display';

  /**
   * @var $module_permission string Module main permission name
   */
  private static $module_permission = 'access layout_display content';
  
  /**
   * @var $storage Array Stored data for use in the module.
   */
  private static $storage = array();
  
  /**
   * @var $defaults Array Default values established for some data.
   */
  private static $defaults = array(
    'config_main_active' => TRUE, 
    'config_layout_active' => TRUE, 
    'config_layout_active_bootstrap' => TRUE, 
    'config_layout_active_extra' => TRUE,
    'config_measures_active' => TRUE, 
    'config_site_active' => TRUE,
    'config_site_identifier' => 'My site identifier', 
    'config_site_key' => 'SITE', 
    'config_site_color' => '334455'
  );

  /**
   * Return module id for operations.
   * 
   * @return string Module identifier
   */
  static function get_id() {
    return self::$module_id;
  }

  /**
   * Return permisssion name for this module.
   *
   * @return string Module permission name
   */
  static function get_permission_name() {
    return self::$module_permission;
  }

  /**
   * Checks if the user has the permission for this module
   * @return bool
   */
  static function user_has_permission() {
    return user_access(self::$module_permission);
  }

  /**
   * Return module's configuration parameters.
   * 
   * @return Array Default options
   */
  static function get_config_values() {

    $defaults = LayoutDisplay::get_defaults();
    $storage = LayoutDisplay::init_storage();
    $default_themes = LayoutDisplay::get_default_activated_themes();

    $values = array();

    $values['config_main_active'] = 
      (!isset($storage['config']['main_active'])) 
      ? $defaults['config_main_active'] 
      : $storage['config']['main_active'];

    $values['config_layout_active'] = 
      (!isset($storage['config']['layout_info']['active'])) 
      ? $defaults['config_layout_active'] : 
      $storage['config']['layout_info']['active'];

    $values['config_layout_active_bootstrap'] = 
      (!isset($storage['config']['layout_info']['bootstrap'])) 
      ? $defaults['config_layout_active_bootstrap'] 
      : $storage['config']['layout_info']['bootstrap'];

    $values['config_layout_active_extra'] =
      (!isset($storage['config']['layout_info']['extra']))
      ? $defaults['config_layout_active_extra']
      : $storage['config']['layout_info']['extra'];

    $values['config_layout_activated_themes'] = 
      (!isset($storage['config']['layout_info']['themes'])) 
      ? $default_themes 
      : $storage['config']['layout_info']['themes'];

    $values['config_measures_active'] = 
      (!isset($storage['config']['measures_info']['active'])) 
      ? $defaults['config_measures_active'] 
      : $storage['config']['measures_info']['active'];

    $values['config_measures_activated_themes'] = 
      (!isset($storage['config']['measures_info']['themes'])) 
      ? $default_themes 
      : $storage['config']['measures_info']['themes'];

    $values['config_site_active'] = 
      (!isset($storage['config']['site_info']['active'])) 
      ? $defaults['config_site_active'] 
      : $storage['config']['site_info']['active'];

    $values['config_site_identifier'] = 
      (!isset($storage['config']['site_info']['identifier'])) 
      ? $defaults['config_site_identifier'] 
      : $storage['config']['site_info']['identifier'];

    $values['config_site_color'] = 
      (!isset($storage['config']['site_info']['color'])) 
      ? $defaults['config_site_color'] 
      : $storage['config']['site_info']['color'];

    $values['config_site_key'] = 
      (!isset($storage['config']['site_info']['key'])) 
      ? $defaults['config_site_key'] 
      : $storage['config']['site_info']['key'];

    $values['config_site_activated_themes'] = 
      (!isset($storage['config']['site_info']['themes'])) 
      ? $default_themes 
      : $storage['config']['site_info']['themes'];

    return $values;

  }

  /**
   * Return default values for module's configuration parameters.
   * 
   * @return Array Default options
   */
  static function get_defaults() {
    $def = self::$defaults;
    $def['config_site_identifier'] = 
      isset($_SERVER['SERVER_NAME']) 
      ? strtoupper($_SERVER['SERVER_NAME'])
      : $def['config_site_identifier'];
    $def['config_site_key'] = 
      isset($_SERVER['SERVER_NAME']) 
      ? strtoupper(substr($_SERVER['SERVER_NAME'], 0, 3)) 
      : $def['config_site_key'];
    return $def;
  }

  /**
   * Return default activated themes (Disable admin theme)
   * 
   * @return Array Default Activated themes
   */
  static function get_default_activated_themes() {
    $storage = self::$storage;
    $enabled_themes = $storage['system']['enabled_themes'];
    $default_activated_themes = $enabled_themes;
    if (isset($storage['system']['admin_theme'])) {
      $admin_theme = $storage['system']['admin_theme'];
      unset($default_activated_themes[$admin_theme]);
    }
    return $default_activated_themes;
  }

  /* ******************************************** */
  /* STORAGE                                      */
  /* ******************************************** */
  
  /**
   * Initialize module private storage.
   */
  static function init_storage() {
    $tmp = variable_get(self::$module_id);
    if (is_array($tmp))
      self::$storage = $tmp;
    return self::$storage;
  }

  /**
   * Return module storage.
   * 
   * @return Array Stored values
   */
  static function get_storage() {
    return self::$storage;
  }

  /**
   * Set module storage
   * 
   * @param $storage Array Data to store
   */
  static function set_storage($storage = array()) {
    self::$storage = $storage;
  }

  /**
   * Save storage on module vars.
   */
  static function save_storage() {
    variable_set(self::$module_id, self::$storage);
  }

  /**
   * Delete all module's stored vars.
   */
  static function remove_storage() {
    variable_del(self::$module_id);
  }

  /* ******************************************** */
  /* SYSTEM ENVIRONMENT                           */
  /* ******************************************** */
  
  /**
   * Establishes enabled themes for theme selector in configuration page.
   * 
   * NOTE: Removes all module's stored data!
   */
  static function reset_system_env() { 
    $admin_theme = variable_get('admin_theme');
    $all_themes = list_themes();
    $enabled_themes = array();
    foreach ($all_themes as $theme_name => $theme_info) {
      if ($theme_info->status or $theme_name == $admin_theme)
        $enabled_themes[$theme_name] = $theme_name;
    }
    // Save in storage.
    $storage = array();
    $storage['system'] = array(
      'enabled_themes' => $enabled_themes, 
      'admin_theme' => $admin_theme
    );
    self::set_storage($storage);
    self::save_storage();
  }

}
