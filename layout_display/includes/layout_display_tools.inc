<?php

/**
 * Converts hexadecimal color to its RGB integer values.
 *
 * @param
 *        Hexadecimal color $colorname
 * @return array RGB Integer values (0-255)
 */
function _layout_display_hextorgb($colorName) {
  return list($r, $g, $b) = array_map('hexdec', str_split($colorName, 2));
}